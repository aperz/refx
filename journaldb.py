'''
Create a database of all possible jornal abbreviations
'''

import requests
from bs4 import BeautifulSoup

def get_all_abbreviations():
    pages = ["0-9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    url_template = "http://images.webofknowledge.com/images/help/WOS/{0}_abrvjt.html"

    A = []
    for page in pages:
        print("Working on", page)
        url = url_template.format(page)
        h = requests.get(url)
        c = h.content
        h.close()
        soup = BeautifulSoup(c.decode(), "lxml")

        a = [i.text.strip() for i in soup.find_all('dd')]
        a = set([i for i in a if len(i)>1 and len(i)<30])
        [A.append(i) for i in a]
        A = list(set(A))
    return A


def save_journaldb(path):
    A = get_all_abbreviations()
    with open(path, "w") as f:
        for i in A:
            f.write(i)
,
