#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

This file needs some cleaning - which packages are actually needed / used?

packages I've tried:
cermine
pdffigures2

pdftables.pdftable.get_tables()

tabula-py (import tabula)
"""

# ==============================================================================
# PATTERNS
# ==============================================================================

# journals

#https://stackoverflow.com/questions/4320958/regular-expression-for-recognizing-in-text-citations
author = "(?:[A-Z][A-Za-z'`-]+)"
etal = "(?:et al.?)"
additional = "(?:,? (?:(?:and |& )?" + author + "|" + etal + "))"
year_num = "(?:19|20)[0-9][0-9]"
page_num = "(?:, p.? [0-9]+)?"  # Always optional
year = "(?:, *"+year_num+page_num+"| *\("+year_num+page_num+"\))"
#year = " *\("+year_num+"\)"

journal = ""
location = "(?:\d+\: ?\d+[-–]\d+)|(?:\d+[,;] \d+[-–]\d+)"
#location = "(\d+[,;] \d+[-–]\d+)"
# https://www.crossref.org/blog/dois-and-matching-regular-expressions/
doi = "/^10.\d{4,9}/[-._;()/:A-Z0-9]+$/i"
pmid = "(?:\[PubMed: \d{4,14}\])"
#end = "(?:"+ location +"|"+ doi +")"

# ? Vol. 26 no. 6
# Science, 305, 1457–1462
# Arch. Dermatol., 142, 1147–1152.
# J. Med. Internet Res., 7, e60.
# Annu. Symp. Proc./AMIA Symp., 919.
# JASIST, doi:10.1002/asi.23538.
# Journal of Theoretical Biology 263: 203–209

#ordinal ="(?:\( ?\d{1,3} ?\) ?)|(?:\[ ?\d{1,3} ?\] ?)|(?:\d{1,3} ?\. ?)"
#ordinal = "(?:\( ?\d{1,3} ?\) ?)|(?:\[ ?\d{1,3} ?\] ?)|(?:\d{1,3} ?\. ?)"
ordinal = "(?:\( ?\d{1,3} ?\) ?|\[ ?\d{1,3} ?\] ?|\d{1,3} ?\. ?)"
#title = "[A-Z][A-Za-z0-9 ]{1,500}"
title = ".{1,500}"

intext_regex = "(" + author + additional+"*" + year + ")"
reference_regex = "^"+  ordinal + "?.{0,5}" + author + additional + "*" + title + year + ".*?" + location +".*$"#+ title + page_num
reference_regex = "^"+ ".{0,5}" + author + additional + "*" + ".*?" + location +".*$"#+ title + page_num
reference_regex = "\n.*?" + year
reference_regex = author + additional + "*$"
# Nature
reference_regex = "^" + ordinal + ".{0,5}" + author #+ additional + "*" + ".*?" + year +".*?$"#+ title + page_num

# ==============================================================================
# PATTERNS
# ==============================================================================

"""
USAGE: see example.py
"""

config = {'verbose': False}

NOTES = """
re.findall('(?<=\"reference\":\[).*?(?=\])', cont) - references for looked-up article?
https://www.crossref.org/labs/resolving-citations-we-dont-need-no-stinkin-parser/
https://github.com/metachris/pdfx/blob/master/pdfx/extractor.py
"""
from Bio import Entrez #install biopython
import re
import os
import random
import time
import itertools
import urllib
import textract
import requests
import sys
import pandas as pd
import numpy as np
import subprocess as sp
import crossref #crossrefapi
import unicodedata
from io import BytesIO
import sqlite3
import pickle
import bs4
from similarity.normalized_levenshtein import NormalizedLevenshtein
#import similarity.normalized_levenshtein.NormalizedLevenshtein as nl

"""
if sys.version_info[0] == 2:
    import scholarly
    import refextract
"""


medlinedb_path = "/D/MEDLINE.db"

Entrez.email='olusiek_p@o2.pl'

reviewed_articles = [
    "Simple tricks of convolutional neural network architectures improve DNA–protein binding prediction",
    "Using two-dimensional convolutional neural network to classify the molecular functions of cytoskeleton motor proteins",
    "Identifying residues that determine palmitoylation using association rule mining",
    "iAcet-Sumo: identification of lysine acetylation and sumoylation sites in proteins by multi-class transformation methods",
    "iRecSpot-EF: Effective Sequence Based Features for Recombination Hotspot Prediction",
    "Global prediction of generic and species-specific succinylation sites by aggregating multiple sequence features",
    "DeepEfflux: a 2D Convolutional Neural Network Model for Identifying Families of Efflux Proteins in Transporters",
    ]

n_citations_true = {
    '30269199': 191,
    '30596896': np.nan,

    '23861384': 50,
    '20596258': 69,
    '24960204': 68,
    '28938573': 90,
    '28334982': 23,
    '20130030': 59,
    '29309530': 15,
    '22699609': 27,

    '19043404': 34,
    '18413326': 30,

}


# ==========
# MEDLINE sqlite queries
# ==========


def citations_for(pmid):
    conn = sqlite3.connect(medlinedb_path)
    c = conn.cursor()
    x = c.execute("select * from citation where source_id is {0}".format(pmid)).fetchall()
    c.close()
    return x

def all_journals():
    conn = sqlite3.connect(medlinedb_path)
    c = conn.cursor()
    x = c.execute("select * from journal").fetchall()
    c.close()
    return pd.DataFrame(x, columns = ['id', 'title', 'ISSN'])

def all_titles(limit=True):
    conn = sqlite3.connect(medlinedb_path)
    c = conn.cursor()
    #colnames = [description[0] for description in c.description]
    if limit:
        x = c.execute("select id,title from document where id > 20000000 limit 99").fetchall()
    else:
        x = c.execute("select * from document").fetchall()
    c.close()
    #x = {k:v.lstrip("[").rstrip("]") for k,v in x.items()}

    return pd.DataFrame(x, columns = ['id', 'title'])



# ==========
# extraction
# ==========
def extract_refs6(text):
    """
    Extract from a block of text that contains only references
    """
    # another idea: create a set of curated matched begin+ends and loop over
    necessary_list = [year_num, additional]
    begin_list=["\s"+ordinal+author,"\s"+author+additional]
    end_list = [year, location, doi, pmid]

    current = ""
    o = []

    for begin in begin_list:
        for end in end_list:
            references_maybe = []

            for i in range(len(t)):
                if re.search(begin, t[i]):
                    current = t[i]
                    #if config['verbose']:
                    #    print("Found BEGIN line:", t[i])
                    #    print(begin)
                    for j in range(0,8): # assume a citation will span between 0 and 10 lines (also check the line itself)
                        if not i+j >= len(t): # end of file

                            if re.search(end, t[i+j]):
                    #            if config['verbose']:
                    #                print("Found END line:", t[i+j])
                    #                print(end)
                                current = current+" "+t[i+j]
                                break
                            else:
                                current = current+" "+t[i+j]


                        else:
                            pass

                    references_maybe.append(current)
                else:
                    continue

            # Discard if doesn't have these elements
            for n in necessary_list:
                references_maybe = [i for i in references_maybe if len(re.findall(n, i)) > 0]

            # Discard if under/over certain length
            references_maybe = [i for i in references_maybe if len(i) > 50]
            references_maybe = [i for i in references_maybe if len(i) < 1200]

            if len(references_maybe) > len(o):
                o = references_maybe


    return o

def extract_refs5(text):
    # another idea: create a set of curated matched begin+ends and loop over
    necessary_list = [year_num, additional]
    begin_list=["^.{0,2}"+ordinal+author,"^\s?\s?"+author]
    end_list = [year+".{0,2}$", location+".{0,2}$", doi+".{0,2}$", pmid+".{0,2}$"]

    # instead
    #has_references = [i for i in re.finditer("references", text, re.I)]
    #if len(has_references) > 0:
    #    t = text[has_references[0].span()[0]: ]
    #    print(len(t))
    #else:
    #    t = text[ int(len(text)/2): ]


    t = text.split('\n')

    has_references = [i for i,l in enumerate(t) if re.fullmatch('references', l, re.I)]
    if len(has_references) > 0:
        t = t[has_references[0]+1: ] # safe
    else:
        t = t[ int(len(t)/2): ]


    current = ""
    o = []

    for begin in begin_list:
        for end in end_list:
            references_maybe = []

            for i in range(len(t)):
                if re.search(begin, t[i]):
                    current = t[i]
                    #if config['verbose']:
                    #    print("Found BEGIN line:", t[i])
                    #    print(begin)
                    for j in range(0,8): # assume a citation will span between 0 and 10 lines (also check the line itself)
                        if not i+j >= len(t): # end of file

                            if re.search(end, t[i+j]):
                    #            if config['verbose']:
                    #                print("Found END line:", t[i+j])
                    #                print(end)
                                current = current+" "+t[i+j]
                                break
                            else:
                                current = current+" "+t[i+j]


                        else:
                            pass

                    references_maybe.append(current)
                else:
                    continue

            # Discard if doesn't have these elements
            for n in necessary_list:
                references_maybe = [i for i in references_maybe if len(re.findall(n, i)) > 0]

            # Discard if under/over certain length
            references_maybe = [i for i in references_maybe if len(i) > 50]
            references_maybe = [i for i in references_maybe if len(i) < 1200]

            if len(references_maybe) > len(o):
                o = references_maybe


    return o


def extract_refs3(text, begin="^"+author, necessary=[year]):
    # record what the "begin" is for each paper?
    '''
    Strategy: filter iteratively. Lenient first step.
    Since  I don't seem to be able to compose a regex to do all in one step
    ALWAYS:
    - starts at the beginning of a new line

    necessary=[year, location+"|"+doi]
    '''

    if re.match("references", text):
        t = text[ [i for i in  re.finditer("references", text, re.I)][-1].span()[-1]: ]
    else:
        t = text[ int(len(text)/2): ]

    t = t.split('\n')

    current = ""
    #begin = "^"+ordinal+author
    #begin = "^"+author
    #end = location+"$"

    references_maybe = []
    #current_citation = 0
    for i,line in enumerate(t):
        matches = re.findall(begin, line)
        if len(matches) > 0:
            current = line
            #print(line)
            for j in range(1,10): # assume a citation will span max 10 lines
                #or end collecting lines when you get to a line ha matches location$
                #if len(re.findall(end, t[i+j])) > 0:

                #if len(re.findall("^"+ordinal +"?" +author, t[i+j])) == 0:
                if not i+j >= len(t): #end of file
                    if len(re.findall(begin, t[i+j])) == 0:
                        current = current+" "+t[i+j]
                    else:
                        current = current+" "+t[i+j]
                        break
                else:
                    pass
            references_maybe.append(current)
            #print("===")
            #print(current)
        else:
            #if int(re.findall("^", ordinal)) ==
            continue

    # Discard if doesn't have these elements
    for n in necessary:
        references_maybe = [i for i in references_maybe if len(re.findall(n, i)) > 0]

    # Discard if under certain length
    references_maybe = [i for i in references_maybe if len(i) > 50]

    # cleaning
    references_maybe = [re.sub("^"+ordinal, "", i).strip() for i in references_maybe]

    return references_maybe


def extract_refs4(text):
    extracted = []
    for begin in ["^"+author, "^"+ordinal+author]:
        x = extract_refs3(text, begin=begin)
        #print(x)
        #[extracted.append(i) for i in x]
        if len(x) > len(extracted):
            extracted = x
    #if len(extracted) < 10:
    #    extracted_old = extract_refs_old(text)
    #    if len(extracted) < len(extracted_old):
    #        extracted = extracted_old
    return extracted



#matches = re.findall(regex, text, re.MULTILINE+re.DOTALL)

# ==========
# misc
# ==========

def clean_query(q):
    q = re.sub('\d', ' ', q)
    q = re.sub('et al', ' ', q)
    q = re.sub('\W', " ", q).strip()
    q = re.sub('(?<= )\w(?= )', ' ', q)
    #q = re.sub('(?<= ) (?= )', '', q)
    q = re.sub(' +', ' ', q)
    #q = re.sub(" ", "[All Fields] AND ", q)+"[All Flields]"
    return q


def show_element(list_of_dicts, elt):
    show = [i[elt] if elt in i.keys() else [] for i in list_of_dicts]
    return show



# ==========
# extraction
# ==========


# refextract

def search(query):
    handle = Entrez.esearch(db='pubmed',
                            sort='relevance',
                            retmax='1',
                            retmode='xml',
                            term=query)
    results = Entrez.read(handle)
    return results


def extract_refs7(filepath):
    #FIXME
    module_path = "/P/refx"
    tmpfile = '.tmpfile.pkl'
    response = sp.call("python2 " + os.path.join(module_path, "refx_py2.py") + " -i " + filepath + " -o " + tmpfile, shell=True)
    assert response == 0, 'Errors executing python 2 code (refx_py2.py).'

    refs = pickle.load(open(tmpfile, "rb"))
    os.remove(tmpfile)
    refs = [i['raw_ref'][0] for i in refs]
    return refs


# based on the [ ] notation

def extract_refs_old(text, filter=True, clean=True, subset=0, is_from_pdf=False):
    #refs_text = rfx.extract_references_from_string(text)
    # for pdfs use
    if is_from_pdf:
        t_ = text.split('\n\n')
        #t = [i.replace('\n', '') for i in t]
        t_ = [i.split('\n') for i in t_]
        t_ = [i for i in itertools.chain(*t_)]
    else:
        t_ = text.split('\n')

    t=[]
    for l in t_:
        if filter:
            if l[:30].find('et al') > 0\
            or len(re.findall("^\[\d+\]", l)) >0\
            or len(re.findall("^\d+\. ", l)) >0\
            or len(re.findall("\((20\d\d.?|19\d\d.?)\)", l)) >0: # improve, see t[0]
                t.append(l)
        else:
            t.append(l)
    # FIXME: len of line: 500 seems like the sensible max for a citation, assuming one per line
    # len of line: 20 seems like the sensible min for a citation
    t = [i for i in t if len(i) > 20 and len(i) < 500 and i != " "]

    if subset > 0:
        t = t[:subset]

    if clean:
        t = [re.sub("^\[\d+\]", "", i).strip() for i in t]
        t = [re.sub("^\d+\.", "", i).strip() for i in t]
        t = [re.sub("\n", "", i).strip() for i in t]

        # more heuristics
        t = [i for i in t if len(i)>30]

    #TODO other ideas for more strict cleaning:
    # There HAS to be a year

    # Current caveats:
    # - If a citation has a newline in it, it will get truncated

    return t




"""
nested for comprehension
refs = [... for inner_list in outer_list for item in inner_list]
---
refs:
"[18]" is extracted as linemarker
"""


# ==========
# lookup
# ==========

def lookup_pmids_pubmed_local(records, ref, evaluation=False):
    """
    WAAAY TOO LONG ANYWAY

    Look up locally
    There must be a better way to do this - this is a naive first pass

    records:
    conn = sqlite3.connect(medlinedb_path)
    c = conn.cursor()
    records = c.execute("select id,title from document where id > 20000000 limit 49").fetchall()
    #records = c.execute("select id,title from document").fetchall()
    c.close()

    """
    nl = NormalizedLevenshtein()
    # improvement 1: parallelize / split?
    s = {}

    if evaluation:
        for pmid,title in records:
            # imporovement 2: string similarity metric / algorithm?
            if isinstance(title, str):
                s[pmid] = (nl.similarity(title, ref), title)
            else:
                continue

    else:
        for pmid,title in records:
            # imporovement 2: string similarity metric / algorithm?
            if isinstance(title, str):
                s[pmid] = nl.similarity(title, ref)
                if s[pmid] > 0.99:
                    return pmid
            else:
                continue


    if evaluation:
        z=[0]
        while s:
            key, value = s.popitem()
            if value[0] > z[0]:
                z = [ value[0],[(key, value[1])] ]
        return z

    else:
        # TODO make fast
        z=[0]
        while s:
            key, value = s.popitem()
            if value > z[0]:
                z = [value,[key]]
            elif value == z[0]:
                z[1].append(key)

        return z


def lookup_pmids_crossref(ref):
    # TODO replace with crossref api pyton package
    #https://github.com/fabiobatalha/crossrefapi

    # frst check if it is in the string:
    naive = re.findall('(?<=doi:).+?(?=\s|$)', ref)
    if len(naive) > 0:
        return naive[0].strip().strip(".")

    crossref_url = "https://api.crossref.org/works?query.bibliographic={0}#"
    #example = "Carberry%2C+Josiah.+%E2%80%9CToward+a+Unified+Theory+of+High-Energy+Metaphysics%3A+Silly+String+Theory.%E2%80%9D+Journal+of+Psychoceramics+5.11+%282008%29%3A+1-3."
    if sys.version_info[0] == 2:
        query = urllib.quote_plus(ref)
    else:
        query = urllib.parse.quote_plus(ref)

    h = requests.get(crossref_url.format(query))
    cont = h.content.decode()
    h.close()

    cont = cont[:3000]

    # the first item should actually be the article itself?
    doi = pd.Series(
        re.findall('(?<=\"DOI\":\").*?(?=\")', cont)
    )[0].replace("\\", "")
    #.value_counts().index[0].replace("\\", "")

    return doi


def lookup_pmid_scholarly(ref):
    query = []
    if 'title' in ref.keys():
        query += [ref['title'][0]] # no title field?
    elif 'misc' in ref.keys():
        query += [ref['misc'][0]]
    elif 'raw_ref' in ref.keys():
        query += [ref['raw_ref'][0]]


    query = " ".join(query)
    query = clean_query(query)
    print(len(query))
    print(query)
    #continue

    if len(query) > 40 and len(query) < 500 :
        search_query = scholarly.search_pubs_query(query)
        try:
            s = next(search_query)
        except StopIteration:
            s = {}
        search_query.close()
        #time.sleep(0.5)
    else:
        s = {}

    print(s)

    results = {}
    results["result"] = s
    results["query"] = query

def lookup_pmids_biopython(refs):
    results = {}
    for i,r in enumerate(refs):
        query = []
        if 'author' in r.keys():
            a = r['author']
            query += a
        if 'misc' in r.keys():
            m = [r['misc'][0]]
            query += m
        query = " ".join(query)
        query = clean_query(query)
        #query = urllib.quote(query)
        #query = query.replace(" ","+")

        print(i)
        print(query)
        #continue

        if len(query) > 0:
            s = dict(search(query))
        else:
            s = {}
        results[i] = s
        results[i]["query"] = query
        #time.sleep(random.randint(1,5))
        #time.sleep(0.5)
    return results

def lookup_pmid_pubmed_online(ref):
    '''
    Search in PubMed using the POST method
    '''
    url_template = "https://www.ncbi.nlm.nih.gov/pubmed/?term={0}"#Cheng+Zhao+iATC+mHyb+hybrid+multi+label+classifier+for+predicting+the+classification+of+anatomical+therapeutic+chemicals+Oncotarget"

    query = []
    #elif 'misc' in ref.keys():
    #    query += [ref['misc'][0]]
    if 'raw_ref' in ref.keys():
        query += [ref['raw_ref'][0]]
    elif 'misc' in ref.keys():
        query += [ref['misc'][0]]

    query = " ".join(query)
    query = clean_query(query)

    if sys.version_info[0] == 2:
        query = urllib.quote_plus(query)
    else:
        query = urllib.parse.quote_plus(query)

    if config['verbose']:
        print("=== .lookup pmid:", query)

    url = url_template.format(query)

    h = requests.get(url)
    cont = h.content
    h.close()

    try:
        title = re.findall('(?<=<title>).*(?=</title>)', cont, flags=re.S)[0].rstrip("  - PubMed - NCBI\n")
    except IndexError:
        title = None

    try:
        pmid = re.findall('(?<=PMID:</dt> <dd>)\d.*?(?=</dd>)', cont, flags=re.I)[0]
    except IndexError:
        pmid = None

    try:
        authors = re.findall('class="auths".*Author information', cont, flags=re.I)[0]
        authors = urllib.unquote(authors)
        authors = re.findall('(?<=href="/pubmed/\?term=).*?(?=\[Author\])', authors)
    except IndexError:
        authors = None

    #TODO: keywords, abstract

    result = {'authors': authors, 'pmid': pmid, 'title': title,
                'query': query}

    return result

def references_for_pmid(pmid):
    pass
    url_template = "https://www.ncbi.nlm.nih.gov/pubmed?LinkName=pubmed_pubmed_refs&from_uid={0}"
    url = url_template.format(pmid)
    h = requests.get(url)
    cont = h.content.decode()
    h.close()
    #TITLES
    # re.findall('(?<=linksrc\=docsum_title\">).*?(?=<)', cont, flags=re.I)
    citations = re.findall('(?<=PMID:</dt> <dd>)\d.*?(?=</dd>)', cont, flags=re.I)[0]
    return citations

def read_pdf_file(filepath):
    #FIXME assume name is PMID...
    text = textract.process(filepath)
    return text


class GrobidResults(object):
    def __init__(self, xml_path):
        self.xml_path = xml_path

        self.xml_string = open(self.xml_path, 'r').read()
        self.soup = bs4.BeautifulSoup(self.xml_string, features="xml")
        self.pmid = os.path.split(self.xml_path.strip(".tei.xml"))[-1]

    def dois(self):
        dois = [i.string for i in self.soup.find_all("idno", type='doi')] #biblStruct, monogr
        return pd.Series(dois)

    def issns(self):
        dois = [i.string for i in self.soup.find_all("idno", type='ISSN')] #biblStruct, monogr
        return pd.Series(dois)



class Articles(object):
    def __init__(self, path, n=0):
        """
        path is a directory with PDF files
        """
        self.path = path
        self.citations = {}
        file_list = [i for i in os.listdir(self.path) if os.path.splitext(i)[-1] in [".PDF", ".pdf"]]
        if n>0:
            file_list = file_list[:n]
        self.file_list = file_list

    def process_grobid(self):
        cwd = os.getcwd()
        odir = os.path.join(cwd, "grobid_output")
        grobid_client_location = "/P/grobid-client-python/"


        if not os.path.exists(odir):
            os.makedirs(odir)

        call = "python grobid-client.py --input {0} --output {1} --consolidate_citations processFulltextDocument"
        #call = "python grobid-client.py --input {0} --output {1} --force --consolidate_citations processFulltextDocument"
        os.chdir(grobid_client_location)
        sp.call(call.format(self.path, odir), shell=True)
        os.chdir(cwd)

        files = os.listdir(odir) #TODO: only "tei.xml"
        for f in files:
            if f.endswith("tei.xml"):
                G = GrobidResults(os.path.join(odir, f))
                self.citations[G.pmid] = Citations(pmid = G.pmid, grobid_results=G)


    def process_all(self):
        self.process_text()
        self.process1()
        self.process2()
        self.process3()

    def process_text(self):
        for f in self.file_list:
            pmid = os.path.splitext(f)[0]
            text = read_pdf_file(os.path.join(self.path,f))
            self.citations[pmid] = Citations(text, filepath = f, is_from_pdf=True, pmid = pmid)

    def process1(self):
        for a,c in self.citations.items():
            print()
            print("="*80)
            print("Extracting references...", a)
            print("="*80)
            c.extract_references(method="latest")#subset=1)
            #c.extract_references(method="v2")
            #c.map_to_doi()
            #c.doi2pmid()
            #c.search_references()
    def process2(self):
        for a,c in self.citations.items():
            print()
            print("="*80)
            print("Mapping to DOIs...", a)
            print("="*80)
            try:
                c.map_to_doi()
            except Exception as e:
                print("An Exception has occurred:", e)
                print("Recording no DOIs found (FIXME).")
                c.dois = pd.Series([])
    def process3(self):
        for a,c in self.citations.items():
            print()
            print("="*80)
            print("Mapping to PMIDs...", a)
            print("="*80)
            try:
                c.doi2pmid(method="pubmed_search")
                #c.doi2pmid(method="ncbi_id_converter")
            except Exception as e:
                #FIXM
                print("An Exception has occurred:", e)
                print("Recording no PMIDs found (FIXME).")
                c.pmids = pd.Series([])

    def list_articles(self):
        return self.citations.keys()

    def list_citations(self):
        pass

    def examine(self, what='text', n=20):
        if what=="text":
            for a,c in self.citations.items():
                print("="*80)
                print(a)
                print("-"*80)
                print(c.text[:500])
                print("="*80)
                print()
        if what=="references_maybe":
            for a,c in self.citations.items():
                print("="*80)
                print(a)
                print("Found", len(c.references_maybe), "potential references.",
                    "(In MedLine:", len(c.medline_citations()), ")")
                print("-"*80)
                print(c.references_maybe[:n])
                print("="*80)
                print()
        if what=="dois":
            for a,c in self.citations.items():
                print("="*80)
                print(a)
                print("Mapped", len(c.dois), "DOIs.",
                    "(In MedLine:", len(c.medline_citations()), ")")
                print("-"*80)
                print(c.dois[:n])
                print("="*80)
                print()
        if what=="pmids":
            for a,c in self.citations.items():
                print("="*80)
                print(a)
                print("Mapped", len(c.pmids), "PMIDs.",
                    "(In MedLine:", len(c.medline_citations()), ")")
                print("-"*80)
                print(c.pmids[:n])
                print("="*80)
                print()

    def frame(self, what='references_maybe'):
        if what=="references_maybe":
            table = {}
            i = 0
            for a,c in self.citations.items():
                for ref in c.references_maybe:
                    table[i] = (a, ref)
                    i+=1
        return pd.DataFrame(table).T

    def counts(self, what = "references_maybe"):
        res = {}
        for a,c in self.citations.items():
            message = str(a) + ": " + "{0}" + "/" + str(len(c.medline_citations()))
            if what == "references_maybe":
                message=message.format(str(len(c.references_maybe)))
            elif what == "dois":
                message=message.format(str(len(c.dois)))
            elif what == "pmids":
                message=message.format(str(len(c.pmids)))

            if a in n_citations_true.keys():
                x = n_citations_true[a]
            else:
                x = "-"
            message+= "/"+str(x)
            print(message)

    def metrics(self):
        oo = {}
        for a,c in self.citations.items():
            try:
                oo[a] = c.metrics()
            except Exception as e:
                print("Articles.metrics: Error",  e)
                pass

            o = pd.DataFrame(oo).T
            o = pd.merge(
                    o,
                    pd.DataFrame(pd.Series(n_citations_true), columns=["n citations true"]),
                    left_index=True, right_index=True, how="left"
                    )
        return o





class Citations(object):
    def __init__(self, text=None, filepath=None, is_from_pdf=False, pmid=np.nan,
                grobid_results=None):
        """
        Examples of what `text` can be:

        From a DOCX file:
        text = textract.process('/home/perza/Downloads/KC Chou reviews.docx')#, encoding="latin-1")
        C = Citations(text)
        C.extract_references(is_from_pdf=False)

        From a PDF file:
        text = textract.process('sample_pdfs/17028-249691-6-PB.pdf')
        C = Citations(text)
        C.extract_references(is_from_pdf=True)

        From a GROBID xml output file:
        C = Citations("sample_pdfs/9999999.pdf")
        """

        self.pmid = pmid
        self.is_from_pdf = is_from_pdf
        self.filepath = filepath
        self.grobid_results = grobid_results

        #if isinstance(grobid_results, GrobidResults):
        if not self.grobid_results is None:
            self.text = self.grobid_results.xml_string #TODO: actual text
            if self.pmid is np.nan:
                self.pmid = self.grobid_results.pmid


        else:
            if isinstance(text, bytes):
                self.text = text.decode()
                self.text = unicodedata.normalize("NFC", self.text)
                #text = text.replace(b"\xc2", b"") # ?
                #text = text.replace(b"\xa0", b"") # no-break space
                #text = text.replace(b"\x0c", b"") # ?
            elif isinstance(text, str):
                self.text = text
            elif text is None:
                self.text = text
            else:
                raise TypeError("Arg text needs to be either bytes or string")



    def extract_references(self, method="latest", subset=0):

        if isinstance(self.grobid_results, GrobidResults):
            #references_maybe = grobid_results.authors
            raise NotImplementedError

        else:
            #references = extract_refs(self.text, filter=True, clean=True, is_from_pdf=self.is_from_pdf, subset = subset)
            if method == "latest":
                assert self.filepath is not None, "refx: No file path provided."
                references = extract_refs7(self.filepath)
            if method == "v5":
                references = extract_refs5(self.text)
            if method == "v4":
                references = extract_refs4(self.text)
            elif method == "v2":
                references = extract_refs_old(self.text)

        self.references_maybe = pd.Series(references).drop_duplicates().dropna()

    """
    def map_to_pmid(self):
        self.pmids = map_to_pmid(self.references_maybe)
        #self.pmids = self.references.apply(map_to_pmid)
    """


    def map_to_doi_mp(self, timeout=None):
        n_jobs = 8
        pool = mp.Pool(processes = n_jobs)

        args = [i[0] for i in self.references_maybe.items()]
        dois = pool.map(lookup_pmids_crossref, args)

        self.dois = pd.Series(self.dois)


    def map_to_doi(self, timeout=None):
        #if isinstance(self.grobid_results, GrobidResults):
        if not self.grobid_results is None:
            print("grobid")
            print(self.grobid_results.dois())
            self.dois = self.grobid_results.dois()

        else:
            if timeout is not None:
                t_end = time.time()+timeout

            self.dois = {}
            #self.reference.apply(lambda ref: lookup_pmids_crossref(ref))
            try:
                for item in self.references_maybe.items():
                    #print(item)

                    if timeout is not None:
                        if time.time() > t_end:
                            break

                    i = item[0]
                    ref = item[1]
                    #print("--- map_to_doi:", i)
                    try:
                        self.dois[i] = lookup_pmids_crossref(ref)
                    except Exception as e:
                        self.dois[i] = np.nan
                        #print("--- map_to_doi: error:", e)
                    #time.sleep(0.5)
                self.dois = pd.Series(self.dois)
            except KeyboardInterrupt:
                self.dois = pd.Series(self.dois)

    def doi2pmid(self, method="pubmed_search"):
        """
        method : "pubmed_search", "ncbi_id_converter"
        """

        #TODO
        #self.pmcids = self.dois.apply(pmcid_from_doi)
        self.pmids = []

        if method == "ncbi_id_converter":
            # for some reason this returns a lot of invalid ids (looking up DOIs)
            url_template = "https://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/?ids={0}&format=csv"

            for doi in self.dois:

                if config['verbose']:
                    print("=== Citations.doi2pmid:", doi)

                if doi is np.nan:
                    continue
                else:
                    h = requests.get(url_template.format(urllib.parse.quote_plus(doi)))
                    cont = h.content
                    h.close()
                    t = pd.read_csv(BytesIO(cont))
                    if not t.shape[0] == 0:
                        pmid = t.PMID[0]
                        self.pmids.append(pmid)
                    else:
                        self.pmids.append(np.nan)


        elif method == "pubmed_search":
            url_template = "https://www.ncbi.nlm.nih.gov/pubmed/?term={0}"

            for doi in self.dois:

                if config['verbose']:
                    print()
                    print("=== Citations.doi2pmid:", doi)

                if doi is np.nan:
                    continue
                else:
                    h = requests.get(url_template.format(urllib.parse.quote_plus(doi)))
                    cont = h.content.decode()
                    h.close()

                    test1 = re.findall("The following terms were not found in PMC", cont)
                    test2 = re.findall("The following term was not found in PMC", cont)
                    if len(test1) > 0 or len(test2)>0:
                        self.pmids.append(np.nan)

                    # PMID:</dt> <dd>19183559</dd>
                    # <input id="absid" type="hidden" value="19183559" />
                    #pmid = re.findall('(?<=PMID:<\/dt> <dd>).*?(?=<\/dd)', cont)
                    pmid = re.findall(
                        '(?<=<input id="absid" type="hidden" value=").*?(?=" \/>)',
                        #'(?<=PMID:<\/dt> <dd>).*?(?=<\/dd)',
                        cont)
                    if len(pmid)>0:
                        if config['verbose']:
                            print("=== doi2pmid: PMID", pmid)
                        self.pmids.append(pmid[0])
                    else:
                        self.pmids.append(np.nan)

        """
                    pmcid = re.findall('(?<=PMCID: <\/dt><dd>).*?(?=<\/dd)', cont)
                    if len(pmcid)>0:
                        if config['verbose']:
                            print("=== doi2pmid: PMCID: ", pmcid)
                        self.pmcids.append(pmcid[0])
                    else:
                        self.pmcids.append(np.nan)
        if hasattr(self, pmcids):
            self.pmcids = pd.Series(self.pmids)
        """

        self.pmids = pd.Series(self.pmids)

    def search_references(self):
        #TODO: can be seriously improved on, e.g. by incorporating scholar and Entrez search
        results = {}
        for i,r in enumerate(self.references_maybe):
            try:
                results[i] = lookup_pmid(r)
                results[i]['reference'] = r
                #time.sleep(0.5)
            except KeyboardInterrupt:
                break
        self.search_results = results

    def get_info(self):
        self._reference_info_tmp = {}

        call = 'curl -LH "Accept: application/x-bibtex" http://dx.doi.org/{0}'

        for item in self.dois.items():
            i = item[0]
            doi = item[1]

            if not doi in self._reference_info_tmp.keys():

                #try:
                lookup = sp.check_output(call.format(doi.replace("(", "\(").replace(")", "\)")),
                            shell=True).decode()
                parsing = lookup.replace('\t', '').replace('\n', '').lstrip("@article{").replace("}}", "}").split(',')[1:-1]
                #print("--- get_info:",parsing)

                parsing2 = {}
                for i in parsing:
                    ls = i.split(' = ')
                    if len(ls) == 2:
                        parsing2[ls[0]] = ls[1]
                    else:
                        parsing2[ls[0]] = np.nan

                self._reference_info_tmp[doi] = parsing2
                #time.sleep(0.5)
                #except Exception as e:
                #    print(e)
                #    self._reference_info_tmp[doi] = np.nan

        self.reference_info = self.dois.apply(lambda doi: self._reference_info_tmp[doi])
        self.reference_info = self.reference_info.apply(lambda info: np.nan if len(info) == 0 else info)


    def values_for(self, key):
        return [r[key] for r in self.search_results.values()]

    def author_count(self):
        #X = itertools.chain(*values_for('authors'))
        X = pd.Series([j for j in itertools.chain.from_iterable([i['authors']
            for i in self.search_results.values() if i['authors']!=None])])
        return X.value_counts()

    def pmid_count(self):
        #X = itertools.chain(*values_for('pmid'))
        X = pd.Series([i['pmid'] for i in self.search_results.values()])
        return X.value_counts()

    def title_count(self):
        #X = itertools.chain(*values_for('pmid'))
        X = pd.Series([i['title'] for i in self.search_results.values()])
        return X.value_counts()

    def medline_citations(self):
        x = set([i[1] for i in citations_for(self.pmid)])
        return x

    def extracted_citations(self):
        # shouldn't be needed
        self.pmids = pd.Series(self.pmids)
        x = set([int(i) for i in self.pmids.dropna().tolist()])
        return x

    def metrics(self):
        y_pred = self.extracted_citations()
        y_true = self.medline_citations()
        # print: number of extracted, number of cit in MEDLINE, TP, FN, ...
        # TN are not well-defined
        # how to define FN? Now it's the number of missed citations
        #TODO review this
        n_true = len(y_true)
        n_pred = len(y_pred)
        TP = len(y_pred.intersection(y_true))
        FN = len(y_true.difference(y_pred)) # missed
        FP = len(y_pred.difference(y_true))

        metr = {
                "n citations extracted": n_pred,
                "n citations in medline": n_true,
                "TP": TP, "FN": FN, "FP": FP,
            }

        #metr =  pd.DataFrame.from_dict(x, orient="index").T
        return metr


    def frame(self, columns= []):
        data = {}
        for c in columns:
            if c == "doi":
                data["doi"] = self.dois
            elif c == "string":
                data["string"] = self.references_maybe
            elif c == "pmcid":
                data["pmcid"] = self.pmcids.apply(lambda x:
                                x[0] if isinstance(x,list) else x)
            elif c == "pmid":
                data["pmid"] = self.pmids.apply(lambda x:
                                x[0] if isinstance(x,list) else x)
            elif c == "extracted_title":
                data["extracted_title"] = self.reference_info.apply(lambda x:\
                                            x['title'].replace("{", "",). replace("}", "")
                                            if isinstance(x, dict) and 'title' in x.keys()
                                            else np.nan)
            elif c == "extracted_authors":
                data["extracted_authors"] = self.reference_info.apply(lambda x:\
                                            x['author'].replace("{", "",). replace("}", "")
                                            if isinstance(x, dict) and 'author' in x.keys()
                                            else np.nan)
                #data["extracted_authors"] = data["extracted_authors"].apply(lambda x:
                #                [i.strip() for i in x.split('and')] if isinstance(x, str) else [x])

                #In [353]: pd.Series(list(set(flat_list))).apply(lambda x: x if isinstance(x,str) and len(re.findall("chou", x, re.I))> 0 else np.nan).dropna()
                #Out[353]:
                #53         K.-C. Chou
                #129     Kuo-Chen Chou
                #133    Kuo-Chen  Chou

            elif c == "is_kcchou":
                data["is_kcchou"] = self.reference_info.apply(lambda x:\
                                            x['author'].replace("{", "",). replace("}", "")
                                            if isinstance(x, dict) and 'author' in x.keys()
                                            else np.nan)
                data["is_kcchou"] = data['is_kcchou'].apply(lambda x: len(re.findall('chou', x, re.I)) > 0 if isinstance(x,str) else np.nan)

            elif c == "extracted_journal":
                data["extracted_journal"] = self.reference_info.apply(lambda x:\
                                            x['journal'].replace("{", "",). replace("}", "")
                                            if isinstance(x, dict) and 'author' in x.keys()
                                            else np.nan)
            else:
                print(c, "is not a valid identifier")



        #return data
        return pd.DataFrame.from_dict(data)[columns]

    def placeholder_fn(self):
        pass




class ArticleSearch(dict):
    def __init__(self, idx):
        self.idx=idx
        pass


