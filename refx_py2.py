#! /usr/local/bin python2

'''
refextract and scholarly only work for python2
'''

import refextract
import pickle


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="\
                        ")
    parser.add_argument("-i", "--input_fp",
                        help="Input PDF file",
                        required=True)
    parser.add_argument("-o", "--output_fp",
                        help="Output file location (pickle file)",
                        required=True)

    args = vars(parser.parse_args())

    ifp = args['input_fp']
    ofp = args['output_fp']

    refs = refextract.extract_references_from_file(ifp)
    with open(ofp, "wb") as f:
        pickle.dump(refs, f)




#def map_to_pmid(list_of_strings):
#    """
#    only python2
#    """
#    import refextract
#    t = list_of_strings
#    refs = [refextract.extract_references_from_string(i, is_only_references=True)
#                for i in t]
#    refs = [i for i in itertools.chain(*refs)] # from iterable
#
#    # filter out low-quality extractions
#    refs = [i for i in refs if len(i.keys()) >2]
#
#    #FIXME: have actual line numbering in place of this (or position?)
#    for i,_ in enumerate(refs):
#        refs[i]['idx'] = i
#
#    return refs
#
#    # If it's the paper that's being reviewed we don't want it:
#    #refs = {k:v for k,v in refs.items() if v.}
#
#def extract_refs2(text, filter=None):
#    '''
#    only for python2
#    '''
#    import refextract
#    refs = refextract.extract_references_from_string(text, is_only_references=True)
#    # filter out low-quality extractions
#    refs = [i for i in refs if len(i.keys()) >2]
#
#    #FIXME: have actual line numbering in place of this (or position?)
#    for i,_ in enumerate(refs):
#        refs[i]['idx'] = i
#
#    return refs
