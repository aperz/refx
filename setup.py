from setuptools import setup, find_packages
#import os

VERSION = '0.1.0'

DEPENDENCIES = [
    "biopython",
   # "itertools",
   # "urllib",
    "textract",
   # "pandas",
   # "numpy",
    "crossrefapi",
    "unicodedata",
]

setup(
    name='refx',
    version=VERSION,
    description='Extraction of scientific references from text',
    url='http://gitlab.com/aperz/refx',
    author='Aleksandra Perz',
    author_email='perz.aleksandra@gmail.com',
    license='',
    packages=find_packages(),
    py_modules=["refx"],
    include_package_data=True,
    install_requires=DEPENDENCIES,
    zip_safe=False,
    )

